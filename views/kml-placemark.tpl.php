<?php
?><Placemark>
  <name>
    <![CDATA[
    <?php print $name ?>
    ]]>
  </name>
  <description>
    <![CDATA[
    <?php print $description ?>
    ]]>
  </description>
  <Point>
    <coordinates><?php print $coords ?></coordinates>
  </Point>
</Placemark>
